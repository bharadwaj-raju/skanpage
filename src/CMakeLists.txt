
include_directories(${skanpage_BINARY_DIR})

set(skanpage_SRCS
    main.cpp 
    Skanpage.cpp
    DocumentModel.cpp
    DocumentSaver.cpp
    DevicesModel.cpp
    OptionsModel.cpp
    SingleOption.cpp
    )

qt5_add_resources(skanpage_SRCS qml.qrc)

ecm_qt_declare_logging_category(skanpage_SRCS
    HEADER skanpage_debug.h
    IDENTIFIER SKANPAGE_LOG
    CATEGORY_NAME org.kde.skanpage
)

add_executable(skanpage ${skanpage_SRCS})

target_link_libraries(skanpage
  PRIVATE
    Qt5::Core
    Qt5::Widgets
    Qt5::Quick
    Qt5::PrintSupport
    Qt5::Qml
    Qt5::QuickControls2
    Qt5::Concurrent
  PRIVATE
    KF5::CoreAddons
    KF5::Sane
    KF5::I18n
    KF5::XmlGui
    KF5::Kirigami2
)


install(TARGETS skanpage ${INSTALL_TARGETS_DEFAULT_ARGS})
